# Node.js 从零开发博客项目

## 介绍

学习《Node.js 从零开发博客项目》课程的记录，包含课程的学习笔记、代码、项目以及问题汇总等资料。

以下是该仓库的目录说明：

```shell
|-- codes: 代码
|-- docs: 笔记
|-- extensions: 扩展
|-- project: 项目
|-- questions: 问题
```

## 笔记

1. Node.js 介绍

   [1.1 Node.js 下载与安装](docs/1-Node.js介绍/1.1-Node.js下载与安装.md)

2. 项目介绍

## 扩展

## 问题

- [Node.js 长期支持版本与当前发布版本的区别](extensions/Node.js长期支持版本和当前发布版本的区别.md)
- [如何更换 nvm 的 Node.js 安装源](extensions/如何更换nvm的安装源.md)

## 建议

如果你有什么好的建议，可以随时通过 [Issues](https://gitee.com/yixinyishi/nodejs-developing-blog/issues) 提出。
