> Node.js 长期支持版本和当前发布版本的区别？

首先，关于 Node.js 为什么会同时支持两个版本这个问题，Node.js 官方[博客](https://nodejs.org/en/blog/community/node-v5/)的一篇文章中进行解释：

*Node.js is growing, and growing fast. As we continue to innovate quickly, we will focus on two different release lines. One release line will fall under our **LTS** plan. All release lines that have LTS support will be even numbers, and (most importantly) focus on stability and security. These release lines are for organizations with complex environments that find it cumbersome to continually upgrade. The other release line is called **Current**. All release lines will be odd numbers, and have a shorter lifespan and more frequent updates to the code. The Current release line will focus on active development of necessary features and refinement of existing APIs.*

翻译过来大概的意思是这样的：

*Node.js 正在增长，而且是快速的增长。随着我们继续快速创新，我们将专注于两个不同的发行版。我们的**LTS**计划将包含一条发布线。所有支持LTS的发行版本号都是偶数，（最重要的是）重点是稳定性和安全性。这些发行版适用于具有复杂环境的组织，这些组织发现持续升级很麻烦。另一个发布行称为**Current**。所有发布行都将是奇数，并且具有较短的寿命和更频繁的代码更新。当前版本的产品线将专注于必要功能的积极开发和现有API的完善。*

在这篇博客文章中还针对这两个版本的使用提供了建议：

- 如果是需要稳定性并拥有复杂的生产环境（例如中型或大型企业），建议使用 **LTS** 版本。
- 如果能够在不干扰环境的情况下快速轻松地升级版本，建议使用 **Current** 版本。

*说明：LTS 表示的就是长期支持版本，Current 表示的就是当前发布版本。*

