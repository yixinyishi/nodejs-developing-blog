> 如何更换nvm的安装源？

nvm 默认使用的 Node.js 安装源是 Node.js 官方提供的安装源：[https://nodejs.org/dist](https://nodejs.org/dist)。使用 Node.js 官方提供的安装源安装 Node.js 经常会遇到两个问题：

- 可能会出现无法访问 Node.js 官方安装源链接的情况
- 在安装过程中可能会出现中断或丢包的情况

nvm 提供了一个变量 `$NVM_NODEJS_ORG_MIRROR` 用来配置 Node.js 的安装源，通过这个变量可以更换 Node.js 的安装源。具体方式有两种：

- 通过配置 `$NVM_NODEJS_ORG_MIRROR` 变量的值，一次性更换 Node.js  的安装源

  ```shell
  export NVM_NODEJS_ORG_MIRROR=https://nodejs.org/dist
  nvm install node
  ```

- 在每次通过 nvm 安装 Node.js 时通过配置 `$NVM_NODEJS_ORG_MIRROR` 变量的值动态指定 Node.js 安装源

  ```shell
  NVM_NODEJS_ORG_MIRROR=https://nodejs.org/dist nvm install 4.2
  ```

国内阿里云提供了和 Node.js 官方同步的安装源，具体链接是：[http://npm.taobao.org/mirrors/node](http://npm.taobao.org/mirrors/node)。我们可以通过上述变量将 nvm 中 Node.js 的安装源更换成阿里云提供的安装源。

```shell
export NVM_NODEJS_ORG_MIRROR=http://npm.taobao.org/mirrors/node
```

在命令行中执行上述命令就可以成功地更换 Node.js 安装源了。